# README #

### What is this repository for? ###

* Cliente de impresión de códigos de barra.
* Version 2016.08
* Soporta, impresoras como zebra lp 2844, argox

### How do I get set up? ###

* Windows:
    - Java JRE.
    - Token: oOGHRGAHhnZY3Lo2FagdpyseFrlzuWbRzL3ZvILs
    - 1. Configuración avanzada del sistema.
    - 2. Variables de entorno
    - 3. Variable: BBC_DEFAULT
    - 4. Argumentos:
        - q800: Ancho
        - Q160: Alto
        - 24: Padding
        - 1: Columnas