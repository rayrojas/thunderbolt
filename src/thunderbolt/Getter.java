/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderbolt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author bichito
 */
public class Getter {
    private URL url;
    private HttpURLConnection conn;
    private JSONObject response;
    private String token, type_resource = "api", resource, type_auth = "ApiKey", username;

    public Getter(String resource, String token, String username) {
        this.token = token;
        this.resource = resource;
        this.username = username;
    }

    public Getter(String resource, String token, String type_auth, String type_resource) {
        this.token = token;
        this.resource = resource;
        this.type_auth = type_auth;
        this.type_resource = type_resource;
    }

    public JSONObject get_response() {
        return this.response;
    }

    public Getter get() {
        try {
            this.url = new URL(this.resource);
            this.conn = (HttpURLConnection) url.openConnection();
            this.conn.setRequestMethod("GET");
            this.conn.setRequestProperty("Content-Type", "application/json");
            if ("ApiKey".equals(this.type_auth)) {
                this.conn.setRequestProperty("Authorization", "ApiKey " + this.username + ":" + this.token);
            } else {
                this.conn.setRequestProperty("Authorization", "");
            }
            this.conn.setDoOutput(true);
            int codeStatus = conn.getResponseCode();
            if (codeStatus == 200) {
                this.response = this.get_content();
            } else {
                response = new JSONObject("{'meta' : {'status' : " + conn.getResponseCode() + "}}");
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(Getter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Getter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(Getter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this;
    }

    public boolean is_valid() {
        if (this.type_resource == "api") {
            if (!this.response.has("status") || !this.response.has("object")) {
                return false;
            } else {
		return true;
	    }
	}
	return false;
    }
    
    public Map<String, java.util.List<String>> get_header() {
        return this.conn.getHeaderFields();
    }

    private JSONObject get_content() throws IOException, JSONException {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = in.readLine()) != null) {
                stringBuilder.append(line + '\n');
            }
	    System.out.println(stringBuilder.toString());
            JSONTokener tokener = new JSONTokener(stringBuilder.toString());
            JSONObject json = new JSONObject(tokener);
            return json;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }    
}
