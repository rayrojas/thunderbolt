/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderbolt;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.swing.JOptionPane;

/**
 *
 * @author bichito
 */
public class Printer {

    public static void o() {
	String comando_epl = "OD\n"
		+ "S2\n"
		+ "D7\n"
		+ "ZB\n"
		+ "JF\n"
		+ "N\n"
		+ "B44,93,0,1,2,0,34,N,\"882\"\n"
		+ "A0,13,0,2,1,1,N,\"PANTALON\"\n"
		+ "A102,13,0,2,1,1,N,\"casimir\"\n"
		+ "A0,31,0,2,1,1,N,\"Multicolor\"\n"
		+ "A0,51,0,2,1,1,N,\"Casimir\"\n"
		+ "A0,71,0,2,1,1,N,\"28\"\n"
		+ "A95,136,0,2,1,1,N,\"882\"\n"
		+ "A218,45,1,2,1,1,N,\"S/178.00\"\n"
		+ "A569,137,0,2,1,1,N,\"v.1\"\n"
		+ "P" + 1 + "," + 1;
	try {
	    PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
	    byte[] by = comando_epl.getBytes();
	    DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
	    Doc doc = new SimpleDoc(by, flavor, null);
	    DocPrintJob job = printService.createPrintJob();
	    // Imprimir
	    job.print(doc, null);
	} catch (PrintException ex) {
	    JOptionPane.showMessageDialog(null, "Ingreso de datos incorrectos", "Error", JOptionPane.ERROR_MESSAGE);
	}
    }

    public static void print(int arg0, String arg1, String arg2, String arg3, String arg4,
	    String arg5, String arg6, String arg7) {

	String comando_epl = "R0,0\n"
		//****************** Version 1.1 *************************
		+ "N\n"
		+ "ZB\n"
		+ "Q160,24\n"
		+ "q800\n"
		+ "S5\n"
		+ "D14\n"
		+ "A" + horizontal(8, 1) + "," + vertical(1) + ",0,1,1,1,N,\"" + arg1 + "\"\n"
		+ "A" + horizontal(8, 2) + "," + vertical(1) + ",0,1,1,1,N,\"" + arg1 + "\"\n"
		+ "A" + horizontal(8, 3) + "," + vertical(1) + ",0,1,1,1,N,\"" + arg1 + "\"\n"
		+ "A" + horizontal(8, 1) + "," + vertical(2) + ",0,1,1,1,N,\"" + arg2 + "\"\n"
		+ "A" + horizontal(8, 2) + "," + vertical(2) + ",0,1,1,1,N,\"" + arg2 + "\"\n"
		+ "A" + horizontal(8, 3) + "," + vertical(2) + ",0,1,1,1,N,\"" + arg2 + "\"\n"
		+ "A" + horizontal(8, 1) + "," + vertical(3) + ",0,1,1,1,N,\"" + arg3 + "\"\n"
		+ "A" + horizontal(8, 2) + "," + vertical(3) + ",0,1,1,1,N,\"" + arg3 + "\"\n"
		+ "A" + horizontal(8, 3) + "," + vertical(3) + ",0,1,1,1,N,\"" + arg3 + "\"\n"
		+ "A" + horizontal(8, 1) + "," + vertical(4) + ",0,1,1,1,N,\"" + arg4 + "\"\n"
		+ "A" + horizontal(8, 2) + "," + vertical(4) + ",0,1,1,1,N,\"" + arg4 + "\"\n"
		+ "A" + horizontal(8, 3) + "," + vertical(4) + ",0,1,1,1,N,\"" + arg4 + "\"\n"
		+ "B" + centro_cb(arg5, 1) + "," + vertical(5) + ",0,1B,1,2,40,N,\"" + arg5 + "\"\n"
		+ "B" + centro_cb(arg5, 2) + "," + vertical(5) + ",0,1B,1,2,40,N,\"" + arg5 + "\"\n"
		+ "B" + centro_cb(arg5, 3) + "," + vertical(5) + ",0,1B,1,2,40,N,\"" + arg5 + "\"\n"
		+ "A" + centro(arg5, 1) + "," + vertical(6, 40) + ",0,2,1,1,N,\"" + arg5 + "\"\n"
		+ "A" + centro(arg5, 2) + "," + vertical(6, 40) + ",0,2,1,1,N,\"" + arg5 + "\"\n"
		+ "A" + centro(arg5, 3) + "," + vertical(6, 40) + ",0,2,1,1,N,\"" + arg5 + "\"\n"
		+ "A" + horizontal(230, 1) + "," + vertical(4) + ",1,2,1,1,N,\"" + "S/" + arg6 + "\"\n"
		+ "A" + horizontal(230, 2) + "," + vertical(4) + ",1,2,1,1,N,\"" + "S/" + arg6 + "\"\n"
		+ "A" + horizontal(230, 3) + "," + vertical(4) + ",1,2,1,1,N,\"" + "S/" + arg6 + "\"\n"
		+ "A" + horizontal(8, 1) + "," + vertical(6, 40) + ",0,1,1,1,N,\"" + arg7 + "\"\n"
		+ "A" + horizontal(8, 2) + "," + vertical(6, 40) + ",0,1,1,1,N,\"" + arg7 + "\"\n"
		+ "A" + horizontal(8, 3) + "," + vertical(6, 40) + ",0,1,1,1,N,\"" + arg7 + "\"\n"
		+ "P" + 1 + "," + arg0;
	try {
	    PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
	    byte[] by = comando_epl.getBytes();
	    DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
	    Doc doc = new SimpleDoc(by, flavor, null);
	    DocPrintJob job = printService.createPrintJob();
	    // Imprimir
	    job.print(doc, null);
	} catch (PrintException ex) {
	    JOptionPane.showMessageDialog(null, "Ingreso de datos incorrectos", "Error", JOptionPane.ERROR_MESSAGE);
	}
    }

    public static void print(int arg0, String arg1, String arg2, String arg3, String arg4,
	    String arg5, String arg6, String arg7, int columns) {

	String comando_epl = "R0,0\n"
		//****************** Version 1.1 *************************
		+ "N\n"
		+ "ZB\n"
		+ "Q160,24\n"
		+ "q800\n"
		+ "S5\n"
		+ "D14\n"
		+ first(columns, arg1)
		+ second(columns, arg2)
		+ third(columns, arg3)
		+ fourth(columns, arg4)
		+ fifth(columns, arg5)
		+ sixth(columns, arg5)
		+ seventh(columns, arg6)
		+ eighth(columns, arg7)
		+ "P" + 1 + "," + arg0;
	try {
	    PrintService printService = PrintServiceLookup.lookupDefaultPrintService();
	    byte[] by = comando_epl.getBytes();
	    DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
	    Doc doc = new SimpleDoc(by, flavor, null);
	    DocPrintJob job = printService.createPrintJob();
	    // Imprimir
	    job.print(doc, null);
	} catch (PrintException ex) {
	    JOptionPane.showMessageDialog(null, "Ingreso de datos incorrectos", "Error", JOptionPane.ERROR_MESSAGE);
	}
    }

    private static String eighth(int n, String text) {
	String str = "";
	for (int i = 1; i <= n; i++) {
	    str += "A" + horizontal(8, i) + "," + vertical(6, 40) + ",0,1,1,1,N,\"" + text + "\"\n";
	}
	return str;
    }

    private static String seventh(int n, String text) {
	String str = "";
	for (int i = 1; i <= n; i++) {
	    str += "A" + horizontal(230, i) + "," + vertical(4) + ",1,2,1,1,N,\"" + "S/" + text + "\"\n";
	}
	return str;
    }

    private static String sixth(int n, String text) {
	String str = "";
	for (int i = 1; i <= n; i++) {
	    str += "A" + centro(text, i) + "," + vertical(6, 40) + ",0,2,1,1,N,\"" + text + "\"\n";
	}
	return str;
    }

    private static String fifth(int n, String text) {
	String str = "";
	for (int i = 1; i <= n; i++) {
	    //str += "B" + centro_cb(text, i) + "," + vertical(5) + ",0,1B,1,2,40,N,\"" + text + "\"\n";
	    str += "B" + centro_cb(text, i) + "," + vertical(5) + ",0,1,2,0,34,N,\"" + text + "\"\n";
	    //+ "B44,93,0,1,2,0,34,N,\"882\"\n"
	}
	return str;
    }

    private static String fourth(int n, String text) {
	String str = "";
	for (int i = 1; i <= n; i++) {
	    str += "A" + horizontal(8, i) + "," + vertical(4) + ",0,1,1,1,N,\"" + text + "\"\n";
	}
	return str;
    }

    private static String third(int n, String text) {
	String str = "";
	for (int i = 1; i <= n; i++) {
	    str += "A" + horizontal(8, i) + "," + vertical(3) + ",0,1,1,1,N,\"" + text + "\"\n";
	}
	return str;
    }

    private static String second(int n, String text) {
	String str = "";
	for (int i = 1; i <= n; i++) {
	    str += "A" + horizontal(8, i) + "," + vertical(2) + ",0,1,1,1,N,\"" + text + "\"\n";
	}
	return str;
    }

    private static String first(int n, String text) {
	String str = "";
	for (int i = 1; i <= n; i++) {
	    str += "A" + horizontal(8, i) + "," + vertical(1) + ",0,1,1,1,N,\"" + text + "\"\n";
	}
	return str;
    }

    private static int horizontal(int n, int nlabel) {
	return n + (((nlabel - 1) * 250) + ((nlabel - 1) * 30));
    }

    private static int vertical(int n) {
	if (1 == n) {
	    return 3;
	} else {
	    return ((n - 1) * 16) + 6;
	}
    }

    private static int vertical(int n, int plus) {
	return (((n - 1) * 16) + 6) + plus;
    }

    private static int centro_cb(String txt, int nlabel) {
	double length = (txt.length() * 9) * 1.35;
	int ban = (int) length;
	return (110 - (ban / 2)) + ((nlabel - 1) * 250) + ((nlabel - 1) * 30);
    }

    private static int centro(String txt, int nlabel) {
	int length = txt.length() * 9;
	return (120 - (length / 2)) + ((nlabel - 1) * 250) + ((nlabel - 1) * 30);
    }
}
